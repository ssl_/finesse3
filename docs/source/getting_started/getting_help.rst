.. include:: ../defs.hrst
.. _getting_help:

Getting help
============

For general or technical questions on using Finesse we have a user chat channel
on `Matrix https://matrix.org/`_ `https://matrix.to/#/#finesse:matrix.org https://matrix.to/#/#finesse:matrix.org`_.
This is open to the general public and is the easiest way to get

There is also a mailing list which can also be used for those that prefer email
over Matrix, finesse-users AT nikhef.nl.

For users within the gravitational wave community we also have an `active chat
channel https://chat.ligo.org/ligo/channels/finesse`. This will require you to
log in with your Albert.Einstein credentials.

Finesse and KatScript objects
-----------------------------

Refer to the rest of this documentation for information on the use of KatScript and
Finesse objects.

Interactive help
****************

The :func:`finesse.help` function accepts either a string or object as input and will
show corresponding help. If a string is provided, it is assumed to be a KatScript
:ref:`path <syntax_paths>` like ``m``, ``beamsplitter``, or ``bs.T``. Command names such
as ``xaxis`` are also supported.

This operates similarly to the Python built-in function :func:`help`: either a pager is
opened (if using a console; use :kbd:`PgUp` and :kbd:`PgDn` to navigate, and
:kbd:`q` to escape) or the help text is printed (if using a notebook).
