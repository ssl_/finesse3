"""Shared utilities for Finesse tests."""

from finesse.script.spec import KATSPEC


# Supported KatScript operators.
BINARY_OPERATORS = KATSPEC.binary_operators
UNARY_OPERATORS = KATSPEC.unary_operators
CONSTANTS = KATSPEC.constants
EXPRESSION_FUNCTIONS = KATSPEC.expression_functions
