{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Finesse 3 - Simple Michelson Validation Test\n",
    "\n",
    "Test that Finesse 3 correctly reproduces the response of a simple Michelson interferometer. Defining the distance along to two arms of the interferometer to be $L_x$ and $L_y$, then the common and differential arm lengths are,\n",
    "\\begin{align}\n",
    "\\overline{L} &= \\frac{L_x+L_y}{2},\\\\\n",
    "\\Delta L &= L_x-L_y.\n",
    "\\end{align}\n",
    "\n",
    "![Drawing of the simple Michelson](simple_michelson_drawing.png)\n",
    "\n",
    "We know from equation 5.11 in [1] that the electric field at the antisymetric port for such a michelson is\n",
    "\\begin{equation}\n",
    "E_S = E_0 i e^{2ik \\overline{L}} cos(k\\Delta L)\n",
    "\\end{equation}\n",
    "\n",
    "This simple Michelson is defined to have $1\\,$W of input laser power, $1\\,$m arms and a 50:50 beamsplitter in the following Kat code. The tuning paraameter may then be used to adjust the $x$ mirror position in units of $kL$ degrees.\n",
    "\n",
    "[1] Bond, C., Brown, D., Freise, A. et al. Living Rev Relativ (2016) 19: 3. https://doi.org/10.1007/s41114-016-0002-8"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "     finesse.script.lexer [ WARNING]: Name 'laser' is also the name of a directive, which may lead to confusion.\n"
     ]
    }
   ],
   "source": [
    "import sys, os\n",
    "import finesse\n",
    "import numpy as np\n",
    "\n",
    "kat = finesse.Model()\n",
    "kat.parse(\"\"\"\n",
    "l laser P=1\n",
    "s si laser.p1 BS.p1 L=1\n",
    "bs BS R=0.5 T=0.5\n",
    "s LX BS.p3 mX.p1 L=1\n",
    "m mX R=1 T=0\n",
    "s LY BS.p2 mY.p1 L=1\n",
    "m mY R=1 T=0\n",
    "ad pout BS.p4.o 0\n",
    "xaxis mX.phi lin (-90) 90 200\n",
    "\"\"\")\n",
    "\n",
    "out = kat.run()\n",
    "tuning = np.pi*out.x1/180"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The power response of a michelson is \n",
    "\\begin{equation}\n",
    "P = P_0 cos^2(k\\Delta L).\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Power is real.\n",
      "Maximum Power Error: 1.4e-16W\n",
      "Max relative difference: 1.3877787807814457e-16\n",
      "Power values match analytic.\n"
     ]
    }
   ],
   "source": [
    "power_theory = np.power(np.cos(tuning),2)\n",
    "power_finesse = (out['pout']*np.conj(out['pout']))\n",
    "\n",
    "# The power should be real\n",
    "try:\n",
    "    assert np.all(power_finesse.imag == 0)\n",
    "except AssertionError:\n",
    "    print('Whilst calculating the power from field'\n",
    "          ' amplitude an imaginary power was detected!')\n",
    "    print('Power calculated by taking amp*np.conj(amp)')\n",
    "    print('Power = {}'.format(pd))\n",
    "    sys.exit(15)\n",
    "else:\n",
    "    print('Power is real.')\n",
    "\n",
    "# Compare analytics and Finesse\n",
    "maxerr = np.max(power_theory-power_finesse.real)\n",
    "print('Maximum Power Error: {:.1e}W'.format(maxerr))\n",
    "print('Max relative difference: '+str(maxerr))\n",
    "try:\n",
    "    assert np.allclose(power_finesse,power_theory,\n",
    "                       atol=3e-14,rtol=3e-14)\n",
    "except AssertionError:\n",
    "    print('Power returned from simple michson was not '\n",
    "          'accurate to within 3*10^{-14}.')\n",
    "    sys.exit(15)\n",
    "else:\n",
    "    print('Power values match analytic.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finesse positions optics at $\\lambda \\text{floor}(L/\\lambda)$ for distance $L$ to mitigate floating-point errors. Accounting for this, the electric field at the output photodiode may be computed and the numeric and analytic results compared."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "#< ---- 20 char ---->< ---- 20 char ---->< ---- 20 char ----><9 char->\n",
    "Lambda = 1064e-9\n",
    "k = 2*np.pi/Lambda\n",
    "\n",
    "# Common arm length (in units of angular frequency)\n",
    "common_arm = 0.5*(k*(Lambda*np.floor((1+1)/Lambda))+tuning)\n",
    "\n",
    "# Differential arm length (in units of angular frequency)\n",
    "differential_arm = tuning\n",
    "\n",
    "# Electric field at photodiode (analytics)\n",
    "E_theory = (1j)*np.exp(2j*common_arm)*np.cos(differential_arm)\n",
    "\n",
    "# Electric field at photodide (Finesse)\n",
    "E_finesse = out['pout']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Maximum Amplitude Error: 5.6e-16 sqrt(W)\n",
      "Maximum Phase Error: 2.1e-09 radians\n",
      "Amplitude matches analytics\n",
      "Phase matches analytics\n"
     ]
    }
   ],
   "source": [
    "amplitude_err = np.abs(E_finesse)-np.abs(E_theory)\n",
    "phase_err = np.angle(E_theory)-np.angle(E_finesse)\n",
    "\n",
    "print('Maximum Amplitude Error: {:.1e} sqrt(W)'.format(\n",
    "    np.max(np.abs(amplitude_err))))\n",
    "print('Maximum Phase Error: {:.1e} radians'.format(\n",
    "    np.max(np.abs(phase_err))))\n",
    "\n",
    "try:\n",
    "    assert np.allclose(amplitude_err,0,atol=1e-15,rtol=1e-15)\n",
    "except AssertionError:\n",
    "    print('Electric field amplitude returned from simple '\n",
    "          'Michelson was not accurate to within 3*10^{-15}.')\n",
    "    sys.exit(15)\n",
    "else:\n",
    "    print('Amplitude matches analytics')\n",
    "    \n",
    "try:\n",
    "    assert np.allclose(phase_err,0,atol=1e-8,rtol=1e-15)\n",
    "except AssertionError:\n",
    "    print('Electric field phase returned from simple Michelson'\n",
    "          ' was not accurate to within 3*10^{-8}.')\n",
    "    sys.exit(15)\n",
    "else:\n",
    "    print('Phase matches analytics')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "if 'BESTDATASERVER' in os.environ:\n",
    "    from bestutils import do_upload\n",
    "else:\n",
    "    def do_upload(*args,**kwargs): pass\n",
    "    \n",
    "from matplotlib import pyplot as plt\n",
    "fig, ax = plt.subplots(2,2,sharex=True)\n",
    "ax[0,0].plot(tuning,np.abs(E_theory),'b-',label='Theory')\n",
    "ax[0,0].plot(tuning,np.abs(E_finesse),'r--',label='Finesse')\n",
    "ax[0,0].set_ylabel('Amplitude [$\\sqrt{\\mathrm{W}}$]')\n",
    "\n",
    "ax[0,1].plot(tuning,np.angle(E_theory),'b-')\n",
    "ax[0,1].plot(tuning,np.angle(E_finesse),'r--')\n",
    "ax[0,1].set_ylabel('Phase [rad]')\n",
    "\n",
    "ax[1,0].plot(tuning,amplitude_err,'k')\n",
    "ax[1,0].set_ylabel(r'Amplitude Error [$\\sqrt{\\mathrm{W}}$]')\n",
    "ax[1,0].set_xlabel('Tuning [rad]')\n",
    "\n",
    "ax[1,1].plot(tuning,phase_err,'k')\n",
    "ax[1,1].set_ylabel('Phase Error [rad]')\n",
    "ax[1,1].set_xlabel('Tuning [rad]')\n",
    "for a in ax.flat:\n",
    "    a.grid()\n",
    "\n",
    "fig.legend()\n",
    "fig.suptitle('Simple Michelson Error Behaviour')\n",
    "#fig.tight_layout()\n",
    "file = 'simple_michelson_output.png'\n",
    "fig.savefig(file)\n",
    "plt.close()\n",
    "\n",
    "do_upload(file)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Error behaviour for Finesse3 simple Michelson\n",
    "![Drawing of the Simple Michelson](simple_michelson_output.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "finesse3",
   "language": "python",
   "name": "finesse3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
