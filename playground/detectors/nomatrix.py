import finesse

finesse.LOGGER.setLevel("INFO")

ifo = finesse.Model()
ifo.parse(
    """
l L0 P=1

s s0 L0.p1 ITM.p1

m ITM R=0.99 T=0.01 Rc=-10
s CAV ITM.p2 ETM.p1 L=1
m ETM R=0.99 T=0.01 Rc=10

modes maxtem=0

cav FP ITM.p2.o ITM.p2.i

bp w_cav w ITM.p2.o

xaxis CAV.L 1 10 100
"""
)


out = ifo.run()
out.plot()
