import numpy as np

import finesse
import finesse.analysis as analysis
import finesse.gaussian as gaussian

if __name__ == "__main__":
    from _profile import profile
else:
    from . import profile

IFO = finesse.Model()
IFO.parse(
    """
l L0 P=1
s l0 L0.p1 BS.p1 L=9

# Main beam splitter at 60 deg input AOI
bs BS T=0.5 L=37.5u alpha=60
s BSsub1 BS.p3 BSAR1.p1 L=0.07478 nr=1.44963098985906
s BSsub2 BS.p4 BSAR2.p1 L=0.07478 nr=1.44963098985906
bs BSAR1 R=50u L=0 alpha=-36.6847
bs BSAR2 R=50u L=0 alpha=36.6847

s l_telescope BS.p2 ITM_lens.p1 L=89.6

lens ITM_lens inf
s lITM_th2 ITM_lens.p2 ITMAR.p1 L=0

# Y arm input mirror
m ITMAR R=0 L=20u
s ITMsub ITMAR.p2 ITM.p1 L=0.2 nr=1.44963098985906
m ITM T=7000u L=37.5u Rc=-5580

# Y arm length
s l_arm ITM.p2 ETM.p1 L=10k

# Y arm end mirror
m ETM T=6u L=37.5u Rc=5580
s ETMsub ETM.p2 ETMAR.p1 L=0.2 nr=1.44963098985906
m ETMAR R=0 L=500u

# SRC
s l_SRC BSAR2.p3 SRM.p1 L=11.6

m SRM T=0.2 L=0
s SRMsub SRM.p2 SRMAR.p1 L=0.0749 nr=1.44963098985906
m SRMAR R=0 L=50n

# cavities
cav cavARM ITM.p2.o

lambda 1550n
"""
)

Q_IN = IFO.cavARM.q.reverse()

ITM_TO_BS = IFO.path(IFO.ITM.p2.i, IFO.BS.p2.i)

f = np.linspace(50, 1500, 1000)

profile(
    """
ABCD_to_BS = analysis.abcd(
    IFO,
    path=ITM_TO_BS,
    symbolic=True,
    direction='x'
)
Q_BS = gaussian.transform_beam_param(ABCD_to_BS.M, Q_IN)
w0 = Q_BS.w0.eval(ITM_lens_f=f)
"""
)
