import finesse

if __name__ == "__main__":
    from _profile import profile
else:
    from . import profile

model = finesse.Model()
model.parse(
    """
l L0 P=1

s s0 L0.p1 ITM.p1 L=1

m ITM R=0.9 T=0.1 Rc=-2.5
s CAV ITM.p2 ETM.p1 L=1
m ETM R=0.99 T=0.01 Rc=2.5

modes maxtem=0

cav FP ITM.p2.o ITM.p2.i

cp cav_q FP q
cp cav_w FP w

xaxis ETM.Rcx lin 2.5 10 1000
"""
)

profile()
