import finesse

if __name__ == "__main__":
    from _profile import profile
else:
    from . import profile

model = finesse.Model()
model.parse_legacy_file("../aligo/design.kat")

profile(statement="model.beam_trace()")
