import numpy as np

import finesse
import pykat

# finesse.LOGGER.setLevel("INFO")

CODE = """
l i1 1 0 n1

s s0 0 n1 n1i
gauss g0 i1 n1 870.23867u -5

m m1 0.9 0.1 0 n1i n2
attr m1 Rc -6

gauss g1 m1 n2 800.23867u -5

s s1 10 n2 n3
m m2 0.9 0.1 0 n3 n4
attr m2 Rc 6

maxtem 2
pd trans n4
#phase 0

#xaxis m1 phi lin -48.4 -48 400
noxaxis
yaxis abs:deg
"""

model = finesse.parse(CODE, True)
ifo = model.model
ifo.tag_node(ifo.m2.p2.o, "nTRNS")
ifo.add_all_ad(ifo.nTRNS, 0)
out = model.run()

print("M1.K12 0000 =", ifo.m1.k("K12", 0, 0, 0, 0))
print("M1.K12 0002 =", ifo.m1.k("K12", 0, 0, 0, 2))
print("M1.K12 0200 =", ifo.m1.k("K12", 0, 2, 0, 0))
print("M1.K12 2020 =", ifo.m1.k("K12", 2, 0, 2, 0))

base = pykat.finesse.kat(kat_code=CODE)
base.parse(
    """
trace 32

ad ad00 0 0 0 n4
ad ad01 0 1 0 n4
ad ad02 0 2 0 n4
ad ad10 1 0 0 n4
ad ad20 2 0 0 n4
ad ad11 1 1 0 n4
"""
)
base.verbose = False

outf2 = base.run()
print(outf2.stdout)

print(out["ad00_nTRNS"])
print(outf2["ad00"])
