from finesse.script import parse

kat = parse(
    """
    var f_m 1e5

    l pump P=1e3 f=-&f_m
    s s1 pump.p1 m1.p1
    m m1 T=5.9e-4 L=0
    s scav m1.p2 m2.p1 L=5
    m m2 T=1e-6 L=0

    ad aor m1.p1.o f=&fsig.f-&f_m
    ad aot m2.p2.o f=&fsig.f-&f_m

    fsig 10
    sgen sig1 pump.amp.i 1 0

    xaxis (
        parameter=fsig.f
        mode=lin
        start=-5000+&f_m
        stop=5000+&f_m
        steps=300
    )
    """
)

out = kat.run()
out.plot()
