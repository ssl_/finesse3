import matplotlib.pyplot as plt
import finesse
import pykat

#finesse.LOGGER.setLevel("INFO")

CODE = """
l L0 1 0 n0

s s2 0 n0 nITM1

m ITM 0.99 0.01 0 nITM1 nITM2
s CAV 1 nITM2 nETM1
m ETM 0.99 0.01 0 nETM1 nETM2

attr ITM Rc inf
attr ETM Rc 10
#attr ETM xbeta 10e-6
#attr ETM ybeta 10u

maxtem 4

gauss* gL0 L0 n0 -1 2
cav FP ITM nITM2 ETM nETM1

xaxis ITM phi lin -200 180 400

pd C nETM1
"""

ifo_v2 = pykat.finesse.kat(kat_code=CODE)
ifo_v2.verbose = False
ifo_v2.parse("trace 32")
ifo_v3 = finesse.parse(CODE, True)
#ifo_v3.model.ITM.log_knm(["K11", "K12"])

out_v2 = ifo_v2.run()
#print(out_v2.stdout)
out_v3 = ifo_v3.run()
#print(ifo_v3.last_trace)
#print(ifo_v3.ETM.K11)

fig = plt.figure()
plt.semilogy(out_v2.x, out_v2['C'], label="Finesse 2")
plt.semilogy(out_v3.x1, out_v3['C'], label="Finesse 3")
plt.legend()
plt.show()
