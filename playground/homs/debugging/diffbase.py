import finesse
import pykat

CODE = """
maxtem 12

l i1 1 0 0 nin

gauss g1 i1 nin 0.00184033072815609 -10 # z_R=10m

s s1 1n nin n1

m m1 0 1 0 n1 n2
gauss g2 m1 n2 0.0026026206750103 -10   # z_R=20m

s s2 20 n2 n3

m m2 0 1 0 n3 n4
gauss g3 m2 n4 0.00184033072815609 10 # z_R=10m

ad a1 0 0 0 nin*
ad a2 0 0 0 n4

yaxis abs

xaxis m2 phi lin 0 1 3

trace 8
"""

model = finesse.parse(CODE, True)
out = model.run()
print(model.model.last_trace)

base = pykat.finesse.kat(kat_code=CODE)
base.verbose = False
out_ = base.run()
print(out_.stdout)
