import finesse

finesse.plotting.init()

ifo = finesse.Model()
ifo.parse(
    """
l L0 P=1

s s2 L0.p1 ITM.p1

m ITM R=0.99 T=0.01

gauss gL0 L0.p1.o w0=1m z=0
modes maxtem=4

ccdline refl_scan_line ITM.p1.o npts=400 direction=x x=4 y=0
"""
)

out = finesse.analysis.x2axis(
    ifo.gL0.z, "lin", 0, 2, 50, ifo.ITM.xbeta, "lin", 0, 400e-6, 50,
)
out.plot()
