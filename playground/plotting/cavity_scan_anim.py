import finesse
from finesse.analysis.actions import X3axis

finesse.plotting.init()

#finesse.LOGGER.setLevel("INFO")

model = finesse.Model()
model.parse("""
l L0 P=1

s s0 L0.p1 ITM.p1

m ITM R=0.99 T=0.01 Rc=inf
s CAV ITM.p2 ETM.p1 L=1
m ETM R=0.99 T=0.01 Rc=10

modes(modes="even", maxtem=4)

gauss gL0 L0.p1.o q=(-0.2+2.5j)
cav FP ITM.p2.o ITM.p2.i

#pd refl ITM.p1.o
#pd circ ETM.p1.i
#pd trns ETM.p2.o
ad ad00 ETM.p2.o f=0 n=0 m=0
""")

model.ITM.Rcy = model.ITM.Rcx.ref
out = model.run(X3axis("ITM.phi", 'lin', -180, 180, 50,
                       "ETM.phi", 'lin', -180, 180, 50,
                       "ITM.Rcx", 'lin', -10, -5, 10))

out.plot()
