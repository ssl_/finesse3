import finesse

finesse.plotting.init()

# finesse.LOGGER.setLevel("INFO")

model = finesse.parse(
    """
l L0 P=1

s s0 L0.p1 ITM.p1

m ITM R=0.99 T=0.01 Rc=inf
s CAV ITM.p2 ETM.p1 L=1
m ETM R=0.99 T=0.01 Rc=10

modes even maxtem=4

gauss gL0 L0.p1.o q=(-0.2+2.5j)
cav FP ITM.p2.o ITM.p2.i

pd refl ITM.p1.o
pd circ ETM.p1.i
pd trns ETM.p2.o

xaxis ITM.phi (-180) 180 300
"""
)
out = model.run()

out.plot(figsize_scale=2, logy=True)
