import numpy as np

import finesse
import pykat

# TODO (sjr) Turn this file into a validation test

aLIGO = finesse.Model()
aLIGO.parse_legacy_file("design.kat")
base = pykat.finesse.kat("design.kat")
base.verbose = False
base.parse(
    """"
maxtem 0
noxaxis
"""
)

cavity_ABCDs = {}
cavity_gs = {}
cavity_qs = {}
for cav in aLIGO.cavities:
    cavity_ABCDs[cav.name] = cav.ABCD
    cavity_gs[cav.name] = cav.g
    cavity_qs[cav.name] = cav.q

    base.parse(
        f"""
    cp {cav.name} x A
    cp {cav.name} x B
    cp {cav.name} x C
    cp {cav.name} x D

    cp {cav.name} y A
    cp {cav.name} y B
    cp {cav.name} y C
    cp {cav.name} y D

    cp {cav.name} x stability
    cp {cav.name} y stability

    cp {cav.name} x q
    cp {cav.name} y q

    yaxis re:im
    """
    )

out = base.run()

pykat_cavity_ABCDs = {}
pykat_cavity_gs = {}
pykat_cavity_qs = {}
for cav in aLIGO.cavities:
    Mx = np.array(
        [
            [out[f"{cav.name}_x_A"].real, out[f"{cav.name}_x_B"].real],
            [out[f"{cav.name}_x_C"].real, out[f"{cav.name}_x_D"].real],
        ]
    )
    My = np.array(
        [
            [out[f"{cav.name}_y_A"].real, out[f"{cav.name}_y_B"].real],
            [out[f"{cav.name}_y_C"].real, out[f"{cav.name}_y_D"].real],
        ]
    )

    if np.allclose(Mx, My):
        pykat_cavity_ABCDs[cav.name] = Mx
    else:
        pykat_cavity_ABCDs[cav.name] = Mx, My

    gx = 0.5 * (1 + out[f"{cav.name}_x_stability"].real)
    gy = 0.5 * (1 + out[f"{cav.name}_y_stability"].real)

    if np.isclose(gx, gy):
        pykat_cavity_gs[cav.name] = gx
    else:
        pykat_cavity_gs[cav.name] = gx, gy

    qx = out[f"{cav.name}_x_q"]
    qy = out[f"{cav.name}_y_q"]

    if np.isclose(qx, qy):
        pykat_cavity_qs[cav.name] = qx
    else:
        pykat_cavity_qs[cav.name] = qx, qy


for (cav, ABCD), pABCD in zip(cavity_ABCDs.items(), pykat_cavity_ABCDs.values()):
    if isinstance(ABCD, tuple) and not isinstance(pABCD, tuple):
        print(f"ERROR: {cav} is astigmatic for Finesse 3 but not for Finesse 2")
    if not isinstance(ABCD, tuple) and isinstance(pABCD, tuple):
        print(f"ERROR: {cav} is astigmatic for Finesse 2 but not for Finesse 3")

    if isinstance(ABCD, tuple):
        ABCDx, ABCDy = ABCD
        pABCDx, pABCDy = pABCD

        if not np.allclose(ABCDx, pABCDx):
            print(f"ABCDx unequal for {cav}")
            print(ABCDx)
            print(pABCDx)
        else:
            print(f"ABCDx equal for {cav}")
        if not np.allclose(ABCDy, pABCDy):
            print(f"ABCDy unequal for {cav}")
            print(ABCDy)
            print(pABCDy)
        else:
            print(f"ABCDy equal for {cav}")
    else:
        if not np.allclose(ABCD, pABCD):
            print(f"ABCD unequal for {cav}")
            print(ABCD)
            print(pABCD)
        else:
            print(f"ABCD equal for {cav}")

print()

for (cav, g), pg in zip(cavity_gs.items(), pykat_cavity_gs.values()):
    if isinstance(g, tuple) and not isinstance(pg, tuple):
        print(f"ERROR: {cav} is astigmatic for Finesse 3 but not for Finesse 2")
    if not isinstance(g, tuple) and isinstance(pg, tuple):
        print(f"ERROR: {cav} is astigmatic for Finesse 2 but not for Finesse 3")

    if isinstance(g, tuple):
        gx, gy = g
        pgx, pgy = pg

        if not np.allclose(gx, pgx):
            print(f"gx unequal for {cav}")
            print(gx)
            print(pgx)
        else:
            print(f"gx equal for {cav}")
        if not np.allclose(gy, pgy):
            print(f"gy unequal for {cav}")
            print(gy)
            print(pgy)
        else:
            print(f"gy equal for {cav}")
    else:
        if not np.allclose(g, pg):
            print(f"g unequal for {cav}")
            print(g)
            print(pg)
        else:
            print(f"g equal for {cav}")

print()

for (cav, q), pq in zip(cavity_qs.items(), pykat_cavity_qs.values()):
    if isinstance(q, tuple) and not isinstance(pq, tuple):
        print(f"ERROR: {cav} is astigmatic for Finesse 3 but not for Finesse 2")
    if not isinstance(q, tuple) and isinstance(pq, tuple):
        print(f"ERROR: {cav} is astigmatic for Finesse 2 but not for Finesse 3")

    if isinstance(q, tuple):
        qx, qy = q
        pqx, pqy = pq

        qx = complex(qx)
        qy = complex(qy)

        if not np.allclose(qx, pqx):
            print(f"qx unequal for {cav}")
            print(qx)
            print(pqx)
        else:
            print(f"qx equal for {cav}")
        if not np.allclose(qy, pqy):
            print(f"qy unequal for {cav}")
            print(qy)
            print(pqy)
        else:
            print(f"qy equal for {cav}")
    else:
        q = complex(q)

        if not np.allclose(q, pq):
            print(f"q unequal for {cav}")
            print(q)
            print(pq)
        else:
            print(f"q equal for {cav}")
